import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_top_five_boardgames_categories_for_adults(self):
        df = pd.read_csv('./data.csv')
        result = main.top_five_boardgames_categories_for_adults(df)
        self.assertEqual(result, '[30 28 24 14 14]')

    def test_top_five_boardgames_categories_for_all(self):
        df = pd.read_csv('./data.csv')
        result = main.top_five_boardgames_categories_for_all(df)
        self.assertEqual(result, '[197 178 102  71  58]')
