import matplotlib.pyplot as plt

def top_five_boardgames_categories_for_adults(df):
    df_adults = df[df['age']>=13]
    df_grouped = df_adults.groupby('category').count().sort_values(['names'], ascending=False)
    plt.figure()
    plt.bar(df_grouped.head().index, height=df_grouped.head()['names'])
    plt.xticks(rotation='vertical')
    #plt.show()
    return str(df_grouped.head()['names'].values)

def top_five_boardgames_categories_for_all(df):
    df_grouped = df.groupby('category').count().sort_values(['names'], ascending=False)
    plt.figure()
    plt.bar(df_grouped.head().index, height=df_grouped.head()['names'])
    plt.xticks(rotation='vertical')
    #plt.show()
    return str(df_grouped.head()['names'].values)
