import matplotlib.pyplot  as plt

def find_top_five_rated_books(df):
    df_sorted = df.sort_values(by=['ratings_count'], ascending=False).head()
    #plt.figure()
    #plt.barh(y = df_sorted['title'], width = df_sorted['ratings_count'])
    #plt.title('Top 5 rated books')
    #plt.show()
    return str(df_sorted['title'].head(1).values)

def find_top_average_ratings_books(df):
    upper_half = df.loc[df['ratings_count']>df['ratings_count'].mean()]
    f = upper_half.sort_values(by='average_rating', ascending=False).head()
    return str(f['title'].head(1).values)
