import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_find_top_five_rated_books(self):
        df = pd.read_csv('./data.csv')
        df = df.drop(columns=['Unnamed: 0'])
        result = main.find_top_five_rated_books(df)
        expected = "['Twilight (Twilight  #1)']"
        self.assertEqual(result, expected)

    def test_top_average_ratigns_books(self):
        df = pd.read_csv('./data.csv')
        df = df.drop(columns=['Unnamed: 0'])
        result = main.find_top_average_ratings_books(df)
        expected = "['The Complete Calvin and Hobbes']"
        self.assertEqual(result, expected)

