import matplotlib.pyplot as plt

def get_max_potential_harvest(df):
    df['potential_harvest'] = df['Seeds_Count'] * df['Each_Seed_Produces']
    plt.figure()
    plt.bar(x = df['Vegetable'], height = df['potential_harvest'])
    plt.title('Potential Harvest')
    plt.show()
    return df.sort_values(by = ['potential_harvest'], ascending=[False]).head(1)['potential_harvest'].sum()
