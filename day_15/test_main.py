import unittest
import pandas as pd
import main



class TestMain(unittest.TestCase):
    def test_get_max_potential_harvest(self):
        seeds = {
            'Vegetable' : ['Carrots', 'Tomatoes', 'Potatoes', 'Eggplant', 'Cucumbers'],
            'Seeds_Count' : [300,10,90,100,15],
            'Each_Seed_Produces': [1,140,10,5, 90]
        }
        df = pd.DataFrame(seeds)
        result = main.get_max_potential_harvest(df)
        self.assertEqual(result, 1400)

if __name__ == '__main__':
    unittest.main()
