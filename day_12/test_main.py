import unittest
import main
import pandas as pd

class TestMain(unittest.TestCase):
    def test_filter_avocados(self):
        df = pd.read_csv('./data.csv')
        result = main.filter_avocados(df)
        self.assertEqual(result, 6)

if __name__ == "__main__":
    unittest.main()
