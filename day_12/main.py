def filter_avocados(df):
    filter = df['AveragePrice'] >= 2
    filtered = df[filter]
    group = filtered.groupby(['type', 'year']).mean()
    return group.shape[0]
