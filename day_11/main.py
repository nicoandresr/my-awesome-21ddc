def average_cost_avocado():
    df = pd.read_csv("data.csv")
    df_groupby = df.groupby(['region', 'year']).mean()
    return df_groupby.loc[(2017, 'Albany')]
