import unittest
import main
import data

class TestMain(unittest.TestCase):
    def test_should_buy_by_wholesale(self):
        retail = data.retail_price
        wholesale = data.wholesale_price_list
        amount = data.amount_list
        result = main.should_buy_by_wholesale(retail, wholesale, amount)
        self.assertEqual(result, ['YES', 'YES', 'NO', 'NO'])

if __name__ == '__main__':
    unittest.main()
