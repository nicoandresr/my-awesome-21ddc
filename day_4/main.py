def should_buy_by_wholesale(retail_price, wholesale_price_list, amount_list):
    result = []
    for i in range(0, 4):
        retail = amount_list[i] * retail_price[i]
        buy_by_wholesale = 'NO'
        if retail > wholesale_price_list[i]:
            buy_by_wholesale = 'YES'

        result.append(buy_by_wholesale)
    return result
