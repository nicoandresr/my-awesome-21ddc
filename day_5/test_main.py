import unittest
import main
import data

class TestMain(unittest.TestCase):
    def test_items_over_cost(self):
        city =  data.city_price
        country = data.country_price
        cleaningsupplies = data.cleaningsupplies_list
        result = main.items_over_cost(city, country, cleaningsupplies)
        expected = ['Dustpan', 'Garbage Bags', 'Glass Cleaner', 'Vinegar', 'Floor Cleaner', 'Paper Towels']
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()
