def items_over_cost(city_price, country_price, cleaningsupplies_list):
    result = []
    for i in range(0, len(cleaningsupplies_list)):
        price_diff = (country_price[i] / city_price[i]) - 1

        if price_diff > 0.1:
            result.append(cleaningsupplies_list[i])
    return result
