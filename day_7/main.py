def sort_boxes_by_weight(user_boxes):
    box_sorted = []
    box_list = user_boxes['weight'][:]
    box_list.sort()
    for i in box_list:
        index = user_boxes['weight'].index(i)
        box_sorted.append(user_boxes['box_name'][index])
    return box_sorted
