import unittest
import main

class TestMain(unittest.TestCase):
    def test_sort_boxes_by_weight(self):
        #example dicitonary
        user_boxes = {
            'weight': [4,2,18,21,14,13],
            'box_name': ['box1','box2', 'box3', 'box4', 'box5', 'box6']
        }
        result = main.sort_boxes_by_weight(user_boxes)
        expected = ['box2', 'box1', 'box6', 'box5', 'box3', 'box4']
        self.assertEqual(result, expected)
