import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_total_milk_renevue(self):
        df = pd.read_csv('./data.csv')
        result = main.total_milk_renevue(df)
        self.assertEqual(round(result, 2), 202149.76)

if __name__ == '__main__':
    unittest.main()
