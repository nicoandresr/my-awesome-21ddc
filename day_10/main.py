total_prod = 'Total Milk Production'
num_cows = 'Number of Cows'
pounds = 'Monthly milk production: pounds per cow'
total_renevue = 'Total Revenue'
price_pound = 'Price_Per_Pound'

def total_milk_renevue(df):
    df[total_prod] = df[num_cows] * df[pounds]
    df[total_renevue] = df[total_prod] * df[price_pound]
    return df[total_renevue][155:-1].sum()
