import unittest
import main
import data

class TestMain(unittest.TestCase):
    def test_update_list(self):
        initial = data.initial
        expected = data.expected
        result = main.update_list(initial)
        self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()
