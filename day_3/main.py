def update_list(shopping_list):
    copy_shopping_list = shopping_list[:]
    string_list = ', '.join(copy_shopping_list)
    new_wood_list = string_list.replace('Oak', 'Maple')
    new_paint_list = new_wood_list.replace('White', 'Blue', 2)
    new_shopping_list = new_paint_list.split(', ')
    return new_shopping_list
