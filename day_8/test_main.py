import unittest
import main
import pandas as pd

df = pd.read_csv('milk.csv')

class TestMain(unittest.TestCase):
    def test_get_max_year_month_milk_production(self):
        result = main.get_max_year_month_milk_production(df)
        self.assertEqual(result, '19-Jun')

    def test_get_min_year_month_milk_production(self):
        result = main.get_min_year_month_milk_production(df)
        self.assertEqual(result, '07-Dec')

if __name__ == '__main__':
    unittest.main()
