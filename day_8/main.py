import pandas as pd

def get_max_year_month_milk_production(df):
    idxmax = df['Monthly milk production: pounds per cow'].idxmax()
    return df['Month'].iloc[[idxmax][0]]

def get_min_year_month_milk_production(df):
    idxmin = df['Monthly milk production: pounds per cow'].idxmin()
    return df['Month'].iloc[[idxmin][0]]
