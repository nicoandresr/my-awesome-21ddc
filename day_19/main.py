import matplotlib.pyplot  as plt

def average_time_history(df):
    plt.figure()
    plt.hist(df['avg_time'], bins=40, range=(0, 1000))
    plt.show()
    return 1

def great_average_rating_longer_play_times(df):
    df_filtered = df[df['avg_rating']>=9]
    plt.figure()
    plt.hist(df_filtered['avg_time'], bins=10, range=(0, 600))
    plt.show()
    return 1
