import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_average_time_history(self):
        df = pd.read_csv('./data.csv')
        result = main.average_time_history(df)
        self.assertEqual(result, 1)
    def test_great_average_rating_longer_play_times(self):
        df = pd.read_csv('./data.csv')
        result = main.great_average_rating_longer_play_times(df)
        self.assertEqual(result, 1)

if __name__ == '__main__':
    unittest.main()
