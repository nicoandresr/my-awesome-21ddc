import matplotlib.pyplot  as plt

def count_books_over_four_tousand_pages(df):
    plt.figure()
    plt.boxplot(df['num_pages'], vert = False)
    #plt.show()
    df.describe()
    return df[df['num_pages'] > 4000]['num_pages'].count()

def average_ratings_for_books_over_four_tousand_pages(df):
    return str(df[df['num_pages'] > 4000]['average_rating'].values)
