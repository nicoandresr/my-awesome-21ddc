import unittest
import main
import pandas as pd

class TestMain(unittest.TestCase):
    def test_count_books_over_four_tousand_pages(self):
        df = pd.read_csv('./data.csv')
        df = df.drop(columns=['Unnamed: 0'])
        result = main.count_books_over_four_tousand_pages(df)
        self.assertEqual(result, 2)

    def test_average_ratings_for_books_over_four_tousand_pages(self):
        df = pd.read_csv('./data.csv')
        df = df.drop(columns=['Unnamed: 0'])
        result = main.average_ratings_for_books_over_four_tousand_pages(df)
        self.assertSequenceEqual(result, '[4.7  4.45]')

if __name__ == '__main__':
    unittest.main()
