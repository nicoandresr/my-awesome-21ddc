import unittest
import random 
import main
import data

random.seed(34)

hole_sizes = [random.randint(1, i) for i in range(1, 101)]
random.shuffle(hole_sizes)

# hole sizes in mm
hole_sizes[:5]
hole_list = data.hole_list
hole_cost_to_fix = [1.3, 1.6, 2.1]

class TestMain(unittest.TestCase):
    def test_average_hole_size(self):
        average = main.average_hole_size(hole_list)
        self.assertEqual(average, 28.39)

    def test_average_cost_by_hole(self):
        cost = main.average_cost_to_fix(hole_list, hole_cost_to_fix)
        self.assertEqual(round(cost, 3), 1.488)

    def test_total_cost_to_fix(self):
        total = main.total_cost_to_fix(hole_list, hole_cost_to_fix)
        self.assertEqual(round(total, 3), 148.80)

    def test_maximum_sized_hole(self):
        maximun = main.maximum_sized_hole(hole_list)
        self.assertEqual(maximun, 96)

    def test_minimum_sized_hole(self):
        minimun = main.minimum_sized_hole(hole_list)
        self.assertEqual(minimun, 1)

if __name__ == '__main__':
    unittest.main()
