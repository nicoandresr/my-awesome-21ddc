import pandas as pd # I installed it with pip: pip install pandas

def average_hole_size(hole_list):
    series = pd.Series(hole_list)
    return series.mean()

def total_cost_list(hole_list, hole_cost_to_fix):
    cost_list = []
    for hole in hole_list:
        if hole < 20:
            cost_list.append(hole_cost_to_fix[0])
        elif hole < 70:
            cost_list.append(hole_cost_to_fix[1])
        else:
            cost_list.append(hole_cost_to_fix[2])

    return cost_list

def average_cost_to_fix(hole_list, hole_cost_to_fix):
    series = pd.Series(total_cost_list(hole_list, hole_cost_to_fix))
    return series.mean()
    
def total_cost_to_fix(hole_list, hole_cost_to_fix):
    return sum(total_cost_list(hole_list, hole_cost_to_fix))

def maximum_sized_hole(hole_list):
    series = pd.Series(hole_list)
    return series.max()

def minimum_sized_hole(hole_list):
    series = pd.Series(hole_list)
    return series.min()
