def count_wine_from_stellenbash(df):
    filter = df['region'] == 'Stellenbosch'
    df_filtered = df[filter]
    return df_filtered.value_counts().sum()

def average_price_from_bordeaux(df):
    filter_sulphates = df['sulphates'] <= 0.6
    filter_price = df['price'] < 20
    filter_bordeaux = df['region'] == 'Bordeaux' 
    df_filtered_sulphates = df[filter_sulphates]
    df_filtered_price = df_filtered_sulphates[filter_price]
    df_filtered_bordeax = df_filtered_price[filter_bordeaux]
    return df_filtered_bordeax['price'].mean()

def wine_high_quality_low_price_from_okanagan(df):
    filter_sulphates = df['sulphates'] <= 0.6
    filter_price = df['price'] < 20
    filter_okanagan = df['region'] == 'Okanagan Valley'
    df_filtered_sulphates = df[filter_sulphates]
    df_filtered_price = df_filtered_sulphates[filter_price]
    df_filtered = df_filtered_price[filter_okanagan]
    df_sorted = df_filtered.sort_values(by=['quality', 'price'], ascending=[False, True])
    return df_sorted.head(1)['Unnamed: 0'].sum()
