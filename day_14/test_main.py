import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_count_wine_from_stellenbosh(self):
        df = pd.read_csv('./data.csv')
        result = main.count_wine_from_stellenbash(df)
        self.assertEqual(result, 35)

    def test_average_price_from_bordeaux(self):
        df = pd.read_csv('./data.csv')
        result = main.average_price_from_bordeaux(df)
        self.assertEqual(round(result, 2), 11.3)

    def test_wine_high_quality_low_price_from_okanagan(self):
        df = pd.read_csv('./data.csv')
        result = main.wine_high_quality_low_price_from_okanagan(df)
        self.assertEqual(result, 1025)

if __name__ == '__main__':
    unittest.main()
