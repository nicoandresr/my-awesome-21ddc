col_pounds = 'Monthly milk production: pounds per cow'
col_cows = 'Number of Cows'

def filling_nans(df):
    median_monthly_milk = df[col_pounds].median()
    df[col_pounds] = df[col_pounds].fillna(value = median_monthly_milk)
    df[col_cows] = df[col_cows].fillna(method = 'ffill')
    return df

def average_monthly_milk_production(df):
    return df[col_pounds].mean()

def standard_deviation_for_monthly_milk_production(df):
    return df[col_pounds].std()

def average_number_of_cows(df):
    return df[col_cows].mean()
