import unittest
import pandas as pd
import main

class Test_main(unittest.TestCase):

    def test_average_monthly_milk_production(self):
        df = main.filling_nans(pd.read_csv('./data.csv'))
        result = main.average_monthly_milk_production(df)
        self.assertEqual(round(result, 4), 747.3296)

    def test_standard_average_monthly_milk_production(self):
        df = pd.read_csv('./data.csv')
        result = main.standard_deviation_for_monthly_milk_production(df)
        self.assertEqual(round(result, 4), 92.1778)

    def test_average_number_of_cows(self):
        df = pd.read_csv('./data.csv')
        result = main.average_number_of_cows(df)
        self.assertEqual(round(result), 47.2316)

if __name__ == '__main__':
    unittest.main()
