### DAY 19:
#### Topics:
- Histograms with Matplotlib
#### Challenge
- What type of distribution does the column avg_time have?

- Do games that have a great avg_rating have longer play times?

**Note:** For question 2, filter out games that have are above the avg_rating of 9.0.

##### Stretch

As an optional bonus question, try answering:

- What type of distribution does weight have?

- What happens to the median and mean of a skewed distribution?
##### Solution:
- Right Skewed
- Yes
### DAY 18:
#### Topics:
- Plot styling
#### Challenge

- What are the top 5 boardgame categories in this dataset that are not targeted for young children?
Note: For the question above, use a filter to acquire boardgames with an inteded age of 13+, there is an age column in our dataset.

- Which categories of boardgames that are not targeted for young children are the same compared to the top 5 boardgames categories in the overall dataset?

#### Stretch
- Try out the various matplotlib plot styles from this article.
#### Solution:
- Card Game - Fantasy, Wargame - World War II, Card Game, Fantasy, Economic - Trains
- Card Game - Fantasy, Wargame - World War II, Card Game
### DAY 17:
#### Topics:
- Bar plot
#### Challenge

Help Dot by answering the following questions using a bar plot:

- What are the top 5 rated books in the dataset?

- What are the top 5 books with the highest average rating?

**Note:** Filter out books that have low ratings_count, for question 2 filter out books with a ratings_count less than the mean.

#### Stretch

As an optional bonus question, try answering this as well:

- What are the top 5 authours with the most books in the dataset?

#### Solution:
- Twilight 1, The Hobbit or There and Back Again, The Catcher in the Rye, Angels and Demons, Harry Potter 3
- The Complete Calvin and Hobbles, Harry Potter Boxed Set, It's a Magical World, Harry Potter Collection, The Days are Just Packed

### DAY 16:
#### Topics:
- Boxplot
#### Challenge

Create a boxplot to answer the following questions:

#### Questons:
- How many books have over 4000 pages?

##### Note:
Do not use a fitler, use a boxplot.

- What are the average ratings for books that have over 4000 pages?
##### Note:
You can use a filter for question 2.
#### Solution:
- 2 books
- 4.7 & 4.45

### DAY 15
#### Topics:
- Data Visualization
#### Challenge

Dot already has a few seeds they can use for their garden. They need to figure out which of the seeds will produce the biggest potential harvest. Can you help Dot decide which seeds are best, by using data visualization?

Create a bar graph with Matplotlib that shows each vegetable and the size of the potential harvest that Dot can expect from them.

#### Questions:
- Which of Dot's seeds will produce the largest harvest?
#### Solution:
- Tomatoes

Tomatoes has a potential harvest of 1400

### DAY 14
#### Topics:
- Filters
- Sort
- Group By
#### Challenge

Dot's neighbour said that he only likes wine from Stellenbosch, Bordeaux, and the Okanagan Valley, and that the sulfates can't be that high. The problem is, Dot can't really afford to spend tons of money on the wine. Dot's conditions for searching for wine are:

- Sulfates cannot be higher than 0.6.
- The price has to be less than $20.

Use the above conditions to filter the data for questions 2 and 3 below.

##### Questions:

- Where is Stellenbosch, anyway? How many wines from Stellenbosch are there in the entire dataset?
- After filtering with the 2 conditions, what is the average price of wine from the Bordeaux region?
- After filtering with the 2 conditions, what is the least expensive wine that's of the highest quality from the Okanagan Valley?

##### Solution:
- 35
- 11.3
- 1025

##### Stretch Question:

- What is the average price of wine from Stellenbosch, according to the entire unfiltered dataset?

###### Note:
Check the dataset to see if there are missing values; if there are, fill in missing values with the mean.
