def price_diff(price_a, price_b):
    return round(sum(price_a) / sum(price_b), 4) - 1
    
