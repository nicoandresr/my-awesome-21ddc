import unittest
import main
import data

class Test_main(unittest.TestCase):
    def test_price_diff(self):
        result = main.price_diff(data.city_price, data.country_price)
        expected = 0.2582
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()
