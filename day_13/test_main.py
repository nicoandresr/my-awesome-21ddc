import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_wine_quality(self):
        wine_df = pd.read_csv('./data.csv')
        result = main.wine_quality(wine_df)
        self.assertEqual(result, 18)

    def test_wine_citric(self):
        wine_df = pd.read_csv('./data.csv')
        result = main.wine_citric(wine_df)
        self.assertEqual(result, 105)

if __name__ == '__main__':
    unittest.main()
