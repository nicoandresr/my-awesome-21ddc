def wine_quality(df):
    filter = df['quality'] >= 8
    df_filtered = df[filter]
    df_sorted = df_filtered.sort_values(by=['residual sugar'], ascending=False)
    return df_sorted.shape[0]

def wine_citric(df):
    filter = df['quality'] >= 7
    filter_citric = df['citric acid'] < 0.4
    df_filtered = df[filter]
    df_citric_filtered = df_filtered[filter_citric]
    return df_citric_filtered.shape[0]
