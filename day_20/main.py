import matplotlib.pyplot as plt

def get_correlation(df):
    plt.figure()
    plt.scatter(x = df['weight'], y = df['avg_rating'])
    plt.show()
    return df['weight'].corr(df['avg_rating'])
