import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_get_correlation(self):
        df = pd.read_csv('./data.csv')
        result = main.get_correlation(df)
        self.assertEqual(round(result, 4), 0.5472)

if __name__ == "__main__":
    unittest.main()
