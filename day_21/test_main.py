import unittest
import pandas as pd
import main

class TestMain(unittest.TestCase):
    def test_get_correlation(self):
        df = pd.read_csv('./data.csv')
        result = main.get_correlation(df)
        self.assertEqual(round(result, 4), 0.4786)

    def test_get_top_five_retro(self):
        df = pd.read_csv('./data.csv')
        result = main.get_top_five_retro(df)
        self.assertEqual(result, ['Super Mario Bros.'])

if __name__ == '__main__':
    unittest.main()
