import matplotlib.pyplot as plt

def get_correlation(df):
    C_S = 'Critic_Score'
    df[C_S] = df[C_S].fillna(value=df[C_S].median())
    return df['Critic_Score'].corr(df['User_Score'])

def get_top_five_retro(df):
    df_retro = df[df['Year_of_Release']<2000]
    df_retro.sort_values('Global_Sales', ascending=False)
    top5 = df_retro.head()
    plt.figure()
    plt.bar(top5['Name'],top5['Global_Sales'])
    plt.xticks(rotation='vertical')
    #plt.show()
    return top5['Name'].head(1).values
